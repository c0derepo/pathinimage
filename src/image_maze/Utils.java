package image_maze;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;

public class Utils {

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	private static String RESOURCES_FOLDER = "/resources/", OUTPUT_FOLDER = "src/resources/solved/";

	static BufferedImage imageFromFile(String fileName){

        URL url = Utils.class.getResource(RESOURCES_FOLDER+fileName);
		BufferedImage mazeImage = null;
		try {
		    BufferedImage bufImg = ImageIO.read(url);
		    //force colored image type
		    mazeImage = new BufferedImage(bufImg.getWidth(),
		    					bufImg.getHeight(), BufferedImage.TYPE_INT_ARGB);
		    mazeImage.getGraphics().drawImage(bufImg, 0, 0, null);
		    mazeImage.getGraphics().dispose();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return mazeImage;
	}

	static void imageToFile(Canvas canvas, String fileName, String footerText){

		int footerHeight = 20, inset = 12;
		CountDownLatch latch = new CountDownLatch(1);

		//new bigger canvas that can accommodate footer text
		Canvas newCanvas = new Canvas((int)canvas.getWidth(),	(int)canvas.getHeight()+footerHeight);
		//writable image of the same size
		WritableImage writableImage = new WritableImage((int)canvas.getWidth(),	(int)canvas.getHeight()+footerHeight);

		Platform.runLater(()-> {
			canvas.snapshot(null, writableImage); //dump the canvas to the writable image
			GraphicsContext gc = newCanvas.getGraphicsContext2D(); //get the graphics of the new bigger canvas
			gc.drawImage(writableImage, 0, 0); //draw the writable image onto the bigger canvas graphics
			gc.fillText(footerText, inset, newCanvas.getHeight() - footerHeight + inset); //add text to it
			newCanvas.snapshot(new SnapshotParameters(), writableImage); //overwrite the writable image with the data of the bigger canvas
			latch.countDown();
		});

		try {
			latch.await();
			String fileType = getFileType(fileName);

			/* JPEG and needs BufferedImage.TYPE_INT_RGB. See https://mkyong.com/java/convert-png-to-jpeg-image-file-in-java/
			apparently:
	        BufferedImage bi = SwingFXUtils.fromFXImage(writableImage, null);
	    	returns BufferedImage.TYPE_INT_ARGB encoded buffered image

		    to overcome it force BufferedImage.TYPE_INT_RGB */
			BufferedImage bi = new BufferedImage((int)writableImage.getWidth(), (int)writableImage.getHeight(), BufferedImage.TYPE_INT_RGB);
			SwingFXUtils.fromFXImage(writableImage, bi);

			ImageIO.write(bi, fileType,new File(OUTPUT_FOLDER+fileName) );
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static String getFileType(String fileName) {

		return fileName.substring(fileName.lastIndexOf(".")+1).toLowerCase();
	}

	static WritableImage createFxImage(BufferedImage bi) {
		return SwingFXUtils.toFXImage(bi, null);
	}

	public static void waitMillis(final long millis) {

		try {
			TimeUnit.MILLISECONDS.sleep(millis);

		} catch (final InterruptedException ex) {ex.printStackTrace();}
	}

	public static void waitNanos(final long nanos) {

		try {
			TimeUnit.NANOSECONDS.sleep(nanos);

		} catch (final InterruptedException ex) {ex.printStackTrace();}
	}
}

