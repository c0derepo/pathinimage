/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package image_maze;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;


public class Model {

	public enum State {

		/**
		 * Defines wall color
		 */
		WALL  (Color.BLACK),
		CLEAR  (Color.WHITE) ,                //no activity is or was performed on node
		IS_EXPLORED (new Color(127, 255, 0)), //node is evaluated by path finder
		VISITED (Color.YELLOW),     	      //node was evaluated by path finder
		PATH (Color.RED.brighter()),          //node was evaluated by path finder and added to path
		TARGET (Color.CYAN),
		ORIGIN (Color.PINK);

		public Color color;
		State(Color color) {this.color = color;}

		public State getStateByColor(Color color){
			for(State state : State.values()){
				if(color.equals(state.color)) return state;
			}
			return null;
		}
	}

	/**
	 * Maze image <br/>
	 * Wall color needs to match {@link State#WALL#color}<br/>
	 * Pixel with color different from {@link State#WALL#color} is considered free path
	 */
	private BufferedImage mazeImage;

	/**
	 * Stores all explored points
	 */
	private final HashMap<Point, State> points;

	/**
	 * Image width and height in pixels
	 */
	private int width, height;

	private Point origin, target;

	/**
	 * Use to notify listeners about point {@link State} changes, except {@link State#WALL}
	 * and {@link State#CLEAR}
	 */
	private final SimpleObjectProperty<Map.Entry<Point, State>> stateProperty
																= new SimpleObjectProperty<>();

	private boolean isPrintToFile = false;

	private String mazeName;

	private static final int THRESHOLD = 50;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	public Model(BufferedImage mazeImage) {
		points = new HashMap<>();
		setMazeImage(mazeImage);
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	void setPointState(Point point, State state){

		points.put(point, state);

		//fire change for path, visited, origin, target, is explored
		if(state != State.WALL && state != State.CLEAR) {
			points.entrySet().forEach(e->{
				if(e.getKey().equals(point)) {
					stateProperty.set(e);
					return;
				}
			});
		}
	}

	State getPointState(Point point){

		State state = points.get(point);
		if(state == null){ //add it
			state = getPointStateFromImage(point);
			setPointState(point, state);
		}
		return state;
	}

	private State getPointStateFromImage(Point point){
		return isClear(point) ?  State.CLEAR : State.WALL;
	}

	//determine if a point is clear (passable) wall by comparing its color to clear color
	private boolean isClear(Point point){

	    Color pixelColor = new Color (mazeImage.getRGB((int)point.getX(), (int)point.getY()));

		return Math.abs(pixelColor.getRed() - State.CLEAR.color.getRed()) < THRESHOLD &&
				Math.abs(pixelColor.getGreen()- State.CLEAR.color.getGreen()) < THRESHOLD &&
				Math.abs(pixelColor.getBlue() - State.CLEAR.color.getBlue()) < THRESHOLD ;
	}

	/**
	 * @return true if point is clear or origin or target
	 */
	public boolean isToBeExplored(Point point){

		return  getPointState(point).equals(State.CLEAR) ||
				getPointState(point).equals(State.TARGET) &&  getPointState(point).equals(State.ORIGIN);
	}

	public List<Point> exploredPoints(){
		return new ArrayList<>(points.keySet());
	}

	/**
	 * Clears all information of previously explored points
	 */
	public void clearExploredPoints(){
		points.clear();
	}

	void addStateListener(ChangeListener<Map.Entry<Point, State>> listener) {
		stateProperty.addListener(listener);
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	* Get {@link #mazeImage}
	*/
	public BufferedImage getMazeImage() {
		return mazeImage;
	}

	/**
	 * Setting new image resets {@link #height} and {@link #width}.<br/>
	 * It also invokes {@link #clearExploredPoints()} and sets default origin and target
	 */
	public Model setMazeImage(BufferedImage mazeImage) {
		this.mazeImage = mazeImage;
		setWidth(mazeImage.getWidth());
		setHeight(mazeImage.getHeight());
		setOrigin(new Point(0, 0));
		setTarget(new Point(width, height));
		return this;
	}

	public int getWidth() {
		return width;
	}

	private void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	private void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Default is set to 0,0
	 */
	public Point getOrigin() {
		return origin;
	}

	/**
	 * Default is set to 0,0
	 * Setting new origin invokes {@link #clearExploredPoints()}
	 */
	public Model setOrigin(Point origin) {
		this.origin = origin;
		return this;
	}

	/**
	 * Default is set to {@link #width},{@link #height}
	 */
	public Point getTarget() {
		return target;
	}

	/**
	 * Default is set to {@link #width},{@link #height}
	 * Setting new target invokes {@link #clearExploredPoints()}
	 */
	public Model setTarget(Point target) {
		this.target = target;
		return this;
	}

	public boolean isPrintToFile() {
		return isPrintToFile;
	}

	public Model setPrintToFile(boolean isPrintToFile) {
		this.isPrintToFile = isPrintToFile;
		return this;
	}

	public Model setMazeName(String mazeName) {
		this.mazeName = mazeName;
		return this;
	}

	public String getMazeName() {
		return mazeName;
	}
}
