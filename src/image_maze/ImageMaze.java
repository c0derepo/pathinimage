/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package image_maze;

import java.awt.Point;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import image_maze.Model.State;
import image_maze.TestCase.TestLength;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class ImageMaze extends Application{

	private static final double MAX_PRE_SIZE = 900;

	private final DoubleProperty zoom = new SimpleDoubleProperty(1.0);

	//Single Thread Executor guarantees that task (searches) do not run concurrently
	private final ExecutorService exService = Executors.newSingleThreadExecutor();

	private Algo algo;
	private Model model;
	private GraphicsContext gc;
	private Canvas canvas;
	private TextField info;
	double startSceneX, startSceneY, startTranslateX, startTranslateY;

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@Override
	public void start(Stage stage) throws Exception {

		runTest(3,TestLength.LONGER);
		model.addStateListener((obs, oldV, newV) -> {
			updateImage(newV.getKey(), newV.getValue());
		});

		StackPane imagePane = new StackPane(canvas);

		info = new TextField();
		info.focusTraversableProperty().set(false);
		info.setEditable(false);

		Pane root = new VBox(new ScrollPane(imagePane), info);
		root.setPrefSize(Math.min(canvas.getWidth(), MAX_PRE_SIZE), Math.min(canvas.getHeight(), MAX_PRE_SIZE));
		Scene scene = new Scene(root);
		stage.setTitle("Path in "+model.getMazeName());
		stage.setScene(scene);
		stage.setOnCloseRequest(e -> {
			Platform.exit();
			System.exit(0);
		});

		//canvas drag support
        canvas.setOnMousePressed(canvasOnMousePressedEventHandler());
        canvas.setOnMouseDragged(canvasOnMouseDraggedEventHandler());

		//canvas zoom support
		canvas.scaleXProperty().bind(zoom);
		canvas.scaleYProperty().bind(zoom);

		imagePane.setOnScroll(scrollEvent -> {
			double delta = 0.1;
			double f = scrollEvent.getDeltaY() > 0 ? 1+delta : 1/(1+delta);
			zoom.set(zoom.get() * f);
		});

		stage.show();

		solve();
	}

	private void runTest(int imageNumber , TestLength testLength) {

		TestCase testCase = new TestCase();
		WritableImage writableImage = testCase.configureTest(imageNumber, testLength);
		model = testCase.getModel();
		canvas = new Canvas(writableImage.getWidth(), writableImage.getHeight());
		gc = canvas.getGraphicsContext2D();
		gc.drawImage(writableImage, 0, 0);
	}

	void solve(){

		//algo =  new BfsDfs(model, BfsDfs.Mode.BFS);
		algo =  new AStar(model);

		info.setText("Searching for path using "+ algo.getAlgoName());

		exService.execute(()-> {
			if (algo.solve()) {
				solved();
			} else {
				unSolved();
			}
		});
	}

	private void solved(){
		StringBuilder sb = new StringBuilder(algo.getAlgoName())
								.append(" path length: ").append(algo.getPath().size())
								.append(". ")
								.append(algo.getRunTimeInSeconds()).append(" sec. ")
								.append("Tested: ").append(algo.getNodeCounter())
								.append(" nodes");
		Platform.runLater(()->info.setText(sb.toString()));
		if(model.isPrintToFile()){
			writeSolutionToFile(sb.toString());
		}
	}

	private void unSolved(){

		StringBuilder sb = new StringBuilder(algo.getAlgoName())
							.append(": no pass found. ")
							.append(algo.getRunTimeInSeconds()).append(" sec. ")
							.append("Tested: ").append(algo.getNodeCounter())
							.append(" nodes");
		Platform.runLater(()->info.setText(sb.toString()));
	}

	private void writeSolutionToFile(String footerText){

		new Thread(()->	{//do file writing on a different thread
			Utils.imageToFile(canvas, "solved-" + model.getMazeName(),  footerText);
		}).start();
	}

	private void updateImage(Point point , State state){
		//from awt.Color to javafx.scene.paint.Color
		Color color = Color.rgb(state.color.getRed(), state.color.getGreen(), state.color.getBlue());

		if(state.equals(State.ORIGIN) || state.equals(State.TARGET)){
			gc.setFill(color);
			Platform.runLater(()-> gc.rect(point.x, point.y, 10, 10));
		} else {
			Platform.runLater(()-> gc.getPixelWriter().setColor(point.x, point.y, color));
		}
	}

	 EventHandler<MouseEvent> canvasOnMousePressedEventHandler(){
		 return mouseEvent -> {
		    startSceneX = mouseEvent.getSceneX();
		    startSceneY = mouseEvent.getSceneY();
		    startTranslateX = ((Canvas)mouseEvent.getSource()).getTranslateX();
		    startTranslateY = ((Canvas) mouseEvent.getSource()).getTranslateY();
		};
	 }

	 EventHandler<MouseEvent> canvasOnMouseDraggedEventHandler() {
		 return  mouseEvent -> {
		    double offsetX = mouseEvent.getSceneX() - startSceneX;
		    double offsetY = mouseEvent.getSceneY() - startSceneY;
		    double newTranslateX = startTranslateX + offsetX;
		    double newTranslateY = startTranslateY + offsetY;

		    ((Canvas) mouseEvent.getSource()).setTranslateX(newTranslateX);  //move
		    ((Canvas) mouseEvent.getSource()).setTranslateY(newTranslateY);
		};
}

	public static void main(String[] args){launch(args);}
}
