/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package image_maze;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import image_maze.Model.State;


public class AStar implements Algo{

	private final LinkedList<Point> path;

	private volatile boolean isStopped;
	private final Model model;
	private int nodeCounter;
	private double seconds;
	private long startTime;
	private final String algoName;

	//store cost (path length) to get from origin to point
	private Map<Point, Integer> costs;

	//store the parent (the previous node in the path) of each point
	Map<Point, Point> parents;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	public AStar(Model model) {
		this.model  = model;
		path = new LinkedList<>();
		isStopped = false;
		algoName = "A*";
	};

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@Override
	public boolean solve(){

		if(isStopped) return false;

		nodeCounter = 0;

		Point target = model.getTarget();
		if (target == null || ! isValidPoint(target) || ! model.isToBeExplored(target)) {
			System.err.println("Target is not valid ");
			return false;
		}

		Point origin = model.getOrigin();
		if (origin == null || ! isValidPoint(origin) || ! model.isToBeExplored(origin)){
			System.err.println("Origin is not valid ");
			return false;
		}

		startTime = System.currentTimeMillis();

		parents = new HashMap<>();

		costs = new HashMap<>();

		// queue / stack to hold nodes to be explored
		PriorityQueue<Point> queue = new PriorityQueue<>( (p1 , p2) ->compare(p1 , p2) );

		queue.add(origin);
		updateCostFromOrigin(origin, origin);
		setPointState(origin, State.IS_EXPLORED);

		while (! queue.isEmpty() && ! isStopped) {

			Point exploredPoint = queue.remove();

			if(isSolved(exploredPoint)) {
				seconds = (System.currentTimeMillis() - startTime)/1000;
				solved(parents);
				return true;
			}

			List<Point> nb = getNeighbors(exploredPoint);

			//loop over neighbors
			for(final Point nextPoint : nb){

				if(isStopped) {
					seconds = (System.currentTimeMillis() - startTime)/1000;
					return false;
				}
				//skip if point is wall, or is/was explored or in path
				if(! model.isToBeExplored(nextPoint) ) { continue; }

				parents.put(nextPoint, exploredPoint);
				updateCostFromOrigin(nextPoint, exploredPoint);

				queue.add(nextPoint); //add collection to the queue
				setPointState(nextPoint, State.IS_EXPLORED);
				nodeCounter++;
			}

			setPointState(exploredPoint, State.VISITED);
		}

		seconds = (System.currentTimeMillis() - startTime)/1000;
		return false;
	}


	/**
	 *
	 */
	private int compare(Point p1, Point p2) {

		//cost from origin
		int g1 = costs.get(p1);
		int g2 =  costs.get(p2);

		double f1 =  costToTarget(p1);
		double f2 =  costToTarget(p2);

		return Double.compare(g1+f1, g2+f2);
	}

	/**
	 * Manhattan
	 */
	private double costToTarget(Point point) {
		Point target = model.getTarget();
		return Math.abs(target.y - point.y) + Math.abs(target.x - point.x);
	}

	private void solved(Map<Point, Point> parents) {

		Point pointInPath = model.getTarget();
		while (pointInPath != null){
			addToPath(pointInPath);
			pointInPath = parents.get(pointInPath);
		}

		System.out.println(algoName + ": path length is "+ path.size() + " " +seconds + " sec.");

		//mark origin and target
		setPointState(model.getTarget(), State.TARGET);
		setPointState(model.getOrigin(), State.ORIGIN);
	}

	private void setPointState(Point point, State state) {
		model.setPointState(point, state);
	}


	private void addToPath(Point point) {
		path.push(point);
		setPointState(point, State.PATH);
	}

	private boolean isSolved(Point point) {
		return point.equals(model.getTarget());
	}

	private List<Point> getNeighbors(Point point) {

		List<Point> neighbors = new ArrayList<>();
		int row = point.y, col = point.x;
		for(int[] dir : DIRECTIONS){
			int newRow = row + dir[0] ; int newCol = col + dir[1];
			Point p = new Point(newCol, newRow);
			if(isValidPoint(p)) {
				neighbors.add(p);
			}
		}
		return neighbors;
	}

	private void updateCostFromOrigin(Point point, Point parentPoint) {

		int costOfParent = costs.get(point) == null ?   1 : costs.get(point) ;
		costs.put(point, ++costOfParent );
	}

	@Override
	public void stop(){
		isStopped = true;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public LinkedList<Point> getPath() {
		return path;
	}

	@Override
	public int getNodeCounter() {
		return nodeCounter;
	}

	@Override
	public double getRunTimeInSeconds() {
		return seconds;
	}

	@Override
	public String getAlgoName() {
		return algoName;
	}
}