/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package image_maze;

import java.awt.Point;
import java.util.LinkedList;

public interface Algo {

	//represents moving in 4 directions in the format of {row , column}  (y,x)
	int[][] DIRECTIONS = {{1,0},{-1,0}, {0,1}, {0,-1}}; //down, up, right, left

	boolean solve();

	Model getModel();

	void stop();

	LinkedList<Point> getPath();

	int getNodeCounter();

	double getRunTimeInSeconds();

	default String getAlgoName(){
		return "unknown";
	}

	default boolean isValidPoint (Point point) {
		int row = point.y; int column = point.x;
		return row < getModel().getHeight()-1 && column < getModel().getWidth()-1 && row >= 1 && column >= 1;
	}
}
