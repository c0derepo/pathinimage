/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package image_maze;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import image_maze.Model.State;


public class BfsDfs implements Algo{

	public enum Mode{BFS,DFS}

	private final LinkedList<Point> path;

	private volatile boolean isStopped;
	private final Model model;
	private final Mode mode;
	private int nodeCounter;
	private double seconds;
	private long startTime;
	private String algoName = "unknown" ;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	public BfsDfs(Model model) {
		this.model  = model;
		mode = Mode.BFS;
		algoName = "BFS";
		path = new LinkedList<>();
		isStopped = false;
	};

	public BfsDfs(Model model, Mode mode) {
		this.model  = model;
		this.mode = mode;
		algoName = mode == Mode.BFS ?  "BFS" : "DFS";
		path = new LinkedList<>();
		isStopped = false;
	};

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@Override
	public boolean solve(){

		if(isStopped) return false;

		nodeCounter = 0;

		Point target = model.getTarget();
		if (target == null || ! isValidPoint(target) || ! model.isToBeExplored(target)) {
			System.err.println("Target is not valid ");
			return false;
		}

		Point origin = model.getOrigin();
		if (origin == null || ! isValidPoint(origin) || ! model.isToBeExplored(origin)){
			System.err.println("Origin is not valid ");
			return false;
		}

		startTime = System.currentTimeMillis();

		//store the parent (the previous node in the path) of each point
		Map<Point, Point> parents = new HashMap<>();

		// queue / stack to hold nodes to be explored
		final LinkedList<Point> queue = new LinkedList<>(); //initialize queue
		queue.add(origin);
		setPointState(origin, State.IS_EXPLORED);

		while (! queue.isEmpty() && ! isStopped) {

			Point exploredPoint = mode == Mode.BFS ?  queue.remove() : queue.removeFirst();
//todo		if(exploredPoint == null) return false;

			if(isSolved(exploredPoint)) {
				seconds = (System.currentTimeMillis() - startTime)/1000;
				solved(parents);
				return true;
			}

			List<Point> nb = getNeighbors(exploredPoint);

			//loop over neighbors
			for(final Point nextPoint : nb){

				if(isStopped) {
					seconds = (System.currentTimeMillis() - startTime)/1000;
					return false;
				}

				//skip if point is wall, or is/was explored or in path
				if(! model.isToBeExplored(nextPoint) ) { continue; }

				queue.add(nextPoint); //add collection to the queue
				parents.put(nextPoint, exploredPoint);
				nodeCounter++;
				setPointState(nextPoint, State.IS_EXPLORED);
			}

			seconds = (System.currentTimeMillis() - startTime)/1000;
			setPointState(exploredPoint, State.VISITED);
		}
		return false;
	}

	private void solved(Map<Point, Point> parents) {

		Point pointInPath = model.getTarget();
		while (pointInPath != null){
			addToPath(pointInPath);
			pointInPath = parents.get(pointInPath);
		}

		System.out.println(algoName + ": path length is "+ path.size() + " " +seconds + " sec.");

		//mark origin and target
		setPointState(model.getTarget(), State.TARGET);
		setPointState(model.getOrigin(), State.ORIGIN);
	}

	private void setPointState(Point point, State state) {
		model.setPointState(point, state);
	}

	/**
	 * Append point to path
	 */
	private void addToPath(Point point) {
		path.push(point);
		setPointState(point, State.PATH);
	}

	/**
	 *Is maze solved
	 */
	private boolean isSolved(Point point) {
		return point.equals(model.getTarget());
	}

	private List<Point> getNeighbors(Point point) {

		List<Point> neighbors = new ArrayList<>();
		int row = point.y, col = point.x;
		for(int[] dir : DIRECTIONS){
			int newRow = row + dir[0] ; int newCol = col + dir[1];
			Point p = new Point(newCol, newRow);
			if(isValidPoint(p)) {
				neighbors.add(p);
			}
		}
		return neighbors;
	}

	@Override
	public void stop(){
		isStopped = true;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public LinkedList<Point> getPath() {
		return path;
	}

	@Override
	public int getNodeCounter() {
		return nodeCounter;
	}

	@Override
	public double getRunTimeInSeconds() {
		return seconds;
	}

	@Override
	public String getAlgoName() {
		return algoName;
	}
}