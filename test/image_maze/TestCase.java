/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package image_maze;

import java.awt.Point;
import java.awt.image.BufferedImage;

import javafx.scene.image.WritableImage;

public class TestCase {

	enum TestLength{
		SHORT(0), MEDIUM(1), LONG(2), LONGER(3);
		final int length;
		private TestLength(int length) {
			this.length = length;
		}
	}

	String[] testImages = {"maze1.gif", "maze2.gif", "maze3.jpg", "maze4.jpg"};

	private Model model;

	TestCase() {}

	WritableImage configureTest(int imageNumber, TestLength testLength){

		BufferedImage mazeImage = Utils.imageFromFile(testImages[imageNumber]);
		WritableImage writableImage = Utils.createFxImage(mazeImage);

		model = new Model(mazeImage);
		model.setMazeName(testImages[imageNumber]);

		if(imageNumber == 0 || imageNumber == 1){
			switch (testLength.length){
				case 0:
					//short path
					model.setOrigin(new Point(20, 20)).setTarget(new Point(100,150)).setPrintToFile(true);
					break;
				case 1:
					//medium path
					model.setOrigin(new Point(160, 20)).setTarget(new Point(300,50)).setPrintToFile(true);
					break;
				case 2:
					//long path
					model.setOrigin(new Point(20,20)).setTarget(new Point(300,150)).setPrintToFile(true);
					break;
				case 3: default:
					//long path
					model.setOrigin(new Point(24,30)).setTarget(new Point(400,404)).setPrintToFile(true);
					break;
			}
		}else if(imageNumber == 2 ){//"maze1x2.jpg"
			switch (testLength.length){
				case 0:
					//short path
					model.setOrigin(new Point(20, 20)).setTarget(new Point(200,250)).setPrintToFile(true);
					break;
				case 1:
					//medium path
					model.setOrigin(new Point(20, 20)).setTarget(new Point(200,350)).setPrintToFile(true);
					break;
				case 2:
					//long path
					model.setOrigin(new Point(160, 20)).setTarget(new Point(597,97)).setPrintToFile(true);
					break;
				case 3: default:
					//very long path
					model.setOrigin(new Point(22,28)).setTarget(new Point(797,805)).setPrintToFile(true);
					break;
			}

		}else if(imageNumber == 3 ){//maze4.jpg

			//no spath
			//model.setOrigin(new Point(402,985)).setTarget(new Point(398,20)).setPrintToFile(true);

			switch (testLength.length){
				case 0:
					//short path
					model.setOrigin(new Point(402,985)).setTarget(new Point(429,841)).setPrintToFile(true);
					break;
				case 1:
					//medium path
					model.setOrigin(new Point(402,985)).setTarget(new Point(669,617)).setPrintToFile(true);
					break;
				case 2:
					//long path
					model.setOrigin(new Point(402,985)).setTarget(new Point(329,441)).setPrintToFile(true);
					break;
				case 3: default:
					//very long path
					model.setOrigin(new Point(402,985)).setTarget(new Point(398,26)).setPrintToFile(true);
					break;
			}
		}

		return writableImage;
	}

	Model getModel() {
		return model;
	}
}
